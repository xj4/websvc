# Tiny web-api for sa.

```bash
git clone git@gitlab.com:xj4/websvc.git
cd websvc
go run main.go -addr=0.0.0.0:8008
# Test
curl http://127.0.0.1:8008/id/root
## Binary build
go build
websvc -addr=0.0.0.0:8008

You can custom this code for your daily task, bot api...