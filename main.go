package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"os/exec"

	"github.com/gorilla/mux"
)

type CResponse map[string]interface{}

var servercn = flag.String("addr", "0.0.0.0:82", "a string")

func main() {
	// Init load
	flag.Parse()
	router := mux.NewRouter()
	router.HandleFunc("/id/{user}", func(w http.ResponseWriter, r *http.Request) { idcheck(w, r) })
	fmt.Println("Service listen on: ", *servercn)
	http.ListenAndServe(*servercn, router)

}

func idcheck(res http.ResponseWriter, req *http.Request) {
	msg := CResponse{
		"status": 1,
		"msg":    "",
	}
	vars := mux.Vars(req)
	user := vars["user"]

	cmd := exec.Command("/usr/bin/sh", "id.sh", user)
	out, err := cmd.CombinedOutput()
	fmt.Print(string(out))
	if err == nil {
		fmt.Println("Check id: ", user, " done")
	} else {
		fmt.Sprintf("cmd.Run() failed with %s\n", err)
	}
	response, _ := json.Marshal(msg)
	res.Write(response)
	return

}
